<lancamento>


    <style>

        .menu{
list-style:none; 
border:1px solid #28a745; 
float:left; 
background:#28a745; 
}
.menu li{
position:relative; 
float:left; 
border-right:1px solid #28a745; 
}

 .link{ cursor:pointer;color:#fff; text-decoration:none; padding:5px 10px; display:block;}
 
 .menu li a:hover{
 background:#fff; 
 color:#fff; 
    -moz-box-shadow:0 3px 10px 0 #28a745; 
    -webkit-box-shadow:0 3px 10px 0 #28a745; 
    text-shadow:0px 0px 5px #28a745; 
 }

        


        .teste {
            border: solid 1px black;
            margin-top: 80px;
            border-width: thin;
        }

        .dropdown:hover>.dropdown-menu {
            display: block;
        }

        .lancamentoDiv {
            float: left;
        }
    </style>

    <div class="container d-flex justify-content-center">
        <div class="row col-10 teste">

            <div class="col-2">
                <h5>Lancamento</h5>
            </div>
            <div class="col-2 dropdown">
                <button class="nav-link dropdown-toggle bnt" type="button" id="navbarDropdowLanc" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    +
                </button>
                
                <div class="dropdown-menu" aria-labelledby="navbarDropdownLanc">
                    <a class="dropdown-item nav-link" data-toggle="modal" data-target="#DespesasModal">Despesas</a>
                    <a class="dropdown-item nav-link" data-toggle="modal" data-target="#ReceitaModal">Metas</a>
                    <a class="dropdown-item nav-link" data-toggle="modal" data-target="#lancamentoModal">Lancamento</a>
                </div>
            </div>

            <nav>
                    <ul class="menu">
                          <li><a onclick={meta} class = "link">Meta</a></li>
                          <li><a onclick={lanca} class = "link">Lancamento</a></li>
                          <li><a onclick={divida} class = "link">Dividas</a></li>
                          <li><a onclick={dividaV} class = "link">DivVencidas</a></li>
                          <li><a onclick={metaV} class = "link">MetVencidas</a></li>
                          <li><a data-toggle="modal" data-target="#PagarDivida" class = "link">PagarDivida</a></li>
                          <li><a data-toggle="modal" data-target="#CocluirMeta" class = "link">CocluirMeta</a></li>
                          <li><a data-toggle="modal" data-target="#deletar" class = "link">Deletar</a></li> 
                          <li><a onclick={tabela} class = "link">Relatorios</a></li>
                          <li><a href="#/grafico" class = "link">Graficos</a></li>
                          <li><a onclick={voltar} class = "link">voltar</a></li>                
                  </ul>
                  </nav>
           
            <table class="table">
                </thead>
                <tbody>
                    <tr each = "{me in opts.dados}">
                        <td>ID:{me._id}</td>
                        <td>Descrição:{me.description}</td>
                        <td>Valor:{me.valor}</td>
                        <td>Data:{me.data}</td>
                        
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="DespesasModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabelC" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header modalFundo">
                    <h5 class="modal-title" id="ModalLabelC">Nova despesa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modalFundo">
                    <div class="form-group">
                        <label for="formGroupDescricao">Descrição</label>
                        <input type="text" maxlength="20" class="form-control" id="formGroupDescricaoL" placeholder="Descrição" size=20>
                    </div>

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupValor ">Valor</label>
                        <input type="number" class="form-control" id="formGroupValorL" placeholder="R$ 0,00 " aria-describedby="emailHelp">
                    </div>

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupData">Data</label>
                        <input type="date" class="form-control" id="formGroupDataL" placeholder="19/09/2018 " aria-describedby="emailHelp">
                    </div>


                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupCategoria">Categoria</label>
                        <select class="custom-select" id="categSelect">
                            <option selected>selecione a Categoria</option>
                            <option value="1">Alimentação</option>
                            <option value="2">Saude</option>
                            <option value="3">Despesas_pessoais</option>
                            <option value="4">Educação</option>
                            <option value="5">lazer</option>
                            <option value="6">outros</option>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>
                        <button type="submit" class="btn btn-success" data-dismiss="modal" onclick={cadastrar}>Cadastrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="lancamentoModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabelC" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header modalFundo">
                    <h5 class="modal-title" id="ModalLabelL">Nova despesa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modalFundo">
                    <div class="form-group">
                        <label for="formGroupDescricao">Descrição</label>
                        <input type="text" maxlength="20" class="form-control" id="formGroupDescricaoLL" placeholder="Descrição" size=20>
                    </div>

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupValor ">Valor</label>
                        <input type="number" class="form-control" id="formGroupValorLL" placeholder="R$ 0,00 " aria-describedby="emailHelp">
                    </div>

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupData">Data</label>
                        <input type="date" class="form-control" id="formGroupDataLL" placeholder="19/09/2018 " aria-describedby="emailHelp">
                    </div>

                    

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupCategoria">Categoria</label>
                        <select class="custom-select" id="categSelect1">
                            <option selected>selecione a Categoria</option>
                            <option value="1">Alimentação</option>
                            <option value="2">Saude</option>
                            <option value="3">Despesas_pessoais</option>
                            <option value="4">Educação</option>
                            <option value="5">lazer</option>
                            <option value="6">outros</option>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>
                        <button type="submit" class="btn btn-success" data-dismiss="modal" onclick={cadastrar3}>Cadastrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="ReceitaModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabelC" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header modalFundo">
                    <h5 class="modal-title" id="ModalLabelC">Nova Receita</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modalFundo">
                    <div class="form-group">
                        <label for="formGroupDescricao">Descrição</label>
                        <input type="text" maxlength="20" class="form-control" id="formGroupDescricao" placeholder="Descrição" size=20>
                    </div>

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupValor ">Valor</label>
                        <input type="number" class="form-control" id="formGroupValor" placeholder="R$ 0,00 " aria-describedby="emailHelp">
                    </div>

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupData">Data</label>
                        <input type="date" class="form-control" id="formGroupData" placeholder="19/09/2018 " aria-describedby="emailHelp">
                    </div>

                   

                    <div class="form-group col-md-6 lancamentoDiv">
                        <label for="formGroupCategoria">Categoria</label>
                        <select class="custom-select" id="categSelect2">
                            <option selected>selecione a Categoria</option>
                           <option value="1">Alimentação</option>
                            <option value="2">Saude</option>
                            <option value="3">Despesas_pessoais</option>
                            <option value="4">Educação</option>
                            <option value="5">lazer</option>
                            <option value="6">outros</option>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>
                        <button type="submit" class="btn btn-success" data-dismiss="modal" onclick={cadastrar2}>Cadastrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <div class="modal fade" id="PagarDivida" tabindex="-1" role="dialog" aria-labelledby="ModalLabelC" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header modalFundo">
                        <h5 class="modal-title" id="ModalLabelC">PagarDivida</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
    
                    <div class="modal-body modalFundo">
                        <div class="form-group">
                            <label for="formGroupDivida">IDDivida</label>
                            <input type="text" class="form-control" id="formGroupidDivida" placeholder="IDDivida">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>
                            <button type="submit" class="btn btn-success" data-dismiss="modal" onclick={cadastrar5}>Pagar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    




        <div class="modal fade" id="CocluirMeta" tabindex="-1" role="dialog" aria-labelledby="ModalLabelC" aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-header modalFundo">
                            <h5 class="modal-title" id="ModalLabelC">ConcluirMeta</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
        
                        <div class="modal-body modalFundo">
                            <div class="form-group">
                                <label for="formGroupMetas">IDMeta</label>
                                <input type="text" class="form-control" id="formGroupidMetas" placeholder="IDMeta">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>
                                <button type="submit" class="btn btn-success" data-dismiss="modal" onclick={cadastrar4}>Concluir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="modal fade" id="deletar" tabindex="-1" role="dialog" aria-labelledby="ModalLabelC" aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-header modalFundo">
                            <h5 class="modal-title" id="ModalLabelC">Deletar</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
        
                        <div class="modal-body modalFundo">
                            <div class="form-group">
                                <label for="formGroupMetas">ID</label>
                                <input type="text" class="form-control" id="formGroupidDelete" placeholder="ID">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>
                                <button type="submit" class="btn btn-success" data-dismiss="modal" onclick={deletar}>Concluir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        






    <script>
 
        cadastrar (e){
    var axios = require('axios');
    var i;
    var e = document.getElementById("categSelect");
    var itemSelecionado = e.options[e.selectedIndex].text;
       axios.post('https://ifinancia.herokuapp.com/lancamento/cadastro',{
      "description": document.getElementById("formGroupDescricaoL").value,
      "valor": document.getElementById("formGroupValorL").value,
      "data": document.getElementById("formGroupDataL").value,
      "contaCartao": "teste",
      "categoria": itemSelecionado,
      "tipo":"despesas",
      "cadastro":localStorage.getItem("idusuario")
    })
         .then(function(retrsponse){
           alert("Cadastro efetuado");
            localStorage.setItem("ref","0"); 
       });


       
   
   }


   cadastrar2 (e){
    var axios = require('axios');
    var i;
    var e = document.getElementById("categSelect2");
    var itemSelecionado = e.options[e.selectedIndex].text;
       axios.post('https://ifinancia.herokuapp.com/metas/cadastro',{
      "description": document.getElementById("formGroupDescricao").value,
      "valor": document.getElementById("formGroupValor").value,
      "data": document.getElementById("formGroupData").value,
      "contaCartao": "teste",
      "categoria": itemSelecionado,
      "cadastro":localStorage.getItem("idusuario")
    })
         .then(function(retrsponse){
           alert("Cadastro efetuado");
            localStorage.setItem("ref","0");
          
       });


       
   
   }




   cadastrar3 (e){

  
    localStorage.setItem("valorF",document.getElementById("formGroupValorLL").value);
    var axios = require('axios');
    var i;
    var e = document.getElementById("categSelect1");
    var itemSelecionado = e.options[e.selectedIndex].text;
    var valorM  = 0;

    axios.post('https://ifinancia.herokuapp.com/lancamento/cadastro',{
      "description": document.getElementById("formGroupDescricaoLL").value,
      "valor": document.getElementById("formGroupValorLL").value,
      "data": document.getElementById("formGroupDataLL").value,
      "contaCartao": "teste",
      "categoria":itemSelecionado,
      "tipo": "receita",
      "cadastro":localStorage.getItem("idusuario")
    })
         .then(function(retrsponse){
           
           alert("Cadastro efetuado");
            localStorage.setItem("ref","0");   
           
       });           



var valorS = parseInt(localStorage.getItem("valor"));
var valor2 = localStorage.getItem("valor");
localStorage.setItem("valoratual",valor2);

axios.post('https://ifinancia.herokuapp.com/carteira/contas',{
      "nameConta":"conta2",
      "tipodaconta": "boleto",
      "saldo":(valorS - (valorM = parseInt(localStorage.getItem("valorF")))).toString(),
      "cadastro":localStorage.getItem("idusuario")
    })
         .then(function(response){
           alert("Cadastro carteira");
           
           localStorage.setItem("ref","0");
           
       });

     

       
   
   }



   cadastrar5 (e){
    var axios = require('axios');
    
    for(var i in opts.dados){
        if(opts.dados[i]['_id'] == document.getElementById("formGroupidDivida").value){
            var  valor  = parseInt(opts.dados[i]['valor']);
            var valor2 = parseInt(localStorage.getItem("valor"));
            var valorf = (valor2-valor);
            var valorf2 =  valorf.toString();
             console.log("chablai"); 
        var valor2 = localStorage.getItem("valor");
        localStorage.setItem("valoratual",valor2);
       axios.post('https://ifinancia.herokuapp.com/carteira/contas',{
      "nameConta": "conta",
      "tipodaconta": "teste",
      "saldo":valorf2,
      "cadastro":localStorage.getItem("idusuario") 
    })
         .then(function(response){
           alert("pago com sucesso");
          
           localStorage.setItem("ref","0");
       });

        axios.delete('https://ifinancia.herokuapp.com/lancamento/delete/'+ document.getElementById("formGroupidDivida").value).then(function(response){
    });
            
        }

    }

    
   }


   cadastrar4 (e){
    var axios = require('axios');
    for(var i in opts.dados){
        if(opts.dados[i]['_id'] == document.getElementById("formGroupidMetas").value){
            var  valor  = parseInt(opts.dados[i]['valor']);
            var valor2 = parseInt(localStorage.getItem("valor"));
            var valorf = (valor2-valor);
            var valorf2 =  valorf.toString();
            console.log(valorf2); 
            var valor2 = localStorage.getItem("valor");
            localStorage.setItem("valoratual",valor2);
            
       axios.post('https://ifinancia.herokuapp.com/carteira/contas',{
      "nameConta": "conta",
      "tipodaconta": "teste",
      "saldo":valorf2,
      "cadastro":localStorage.getItem("idusuario") 
    })
         .then(function(response){
           alert("pago com sucesso");
           
           localStorage.setItem("ref","0");
       });

        axios.delete('https://ifinancia.herokuapp.com/metas/delete/'+ document.getElementById("formGroupidMetas").value).then(function(response){
    });
            
        }

    }

    
   }



 voltar(e){


localStorage.setItem("valor","carreg...");
window.location.href = "#/visaogeral";
}

   meta(e){
      
       var axios = require('axios');
   axios.get('https://ifinancia.herokuapp.com/metas/list/'+ localStorage.getItem("idusuario")).then(function(response){
    
      var valores = response.data;
      
      localStorage.setItem("metas", JSON.stringify(valores.metas));
      
      
}); 

var dadosS = localStorage.getItem("metas");


var d = $.parseJSON(dadosS);
console.log(d);
var obj = {};
var date;
var j=0;
var novaData = new Date();
for(var i in d){
    var str = gamb(d[i]['data']);
    date = new Date(str.split('/').join('/'));
    if(date > novaData){
        obj[j] = d[i];
        j++;
    }
      
}

opts.dados = obj;
localStorage.setItem("ref","0");

 

}


 metaV(e){
       
       var axios = require('axios');
       var obj = {};
       var j=0;
   axios.get('https://ifinancia.herokuapp.com/metas/list/'+ localStorage.getItem("idusuario")).then(function(response){
    
      var valores = response.data; 
      localStorage.setItem("metas", JSON.stringify(valores.metas));
      
      
}); 
opts.dados = localStorage.getItem("metas");

var dados = opts.dados;
var dadosS = JSON.parse(dados);




var date;
var novaData = new Date();
for(var i in dadosS){
    var str = gamb(dadosS[i]['data']);
    date = new Date(str.split('/').join('/'));
    if(date < novaData){
        obj[j] = dadosS[i];
        j++;
       
    }
    
      
}
opts.dados = obj;

 

}
 

   lanca(e){
      
       var axios = require('axios');
   axios.get('https://ifinancia.herokuapp.com/lancamento/receitas/'+localStorage.getItem("idusuario")).then(function(response){
    
      var valores = response.data; 
      localStorage.setItem("lanca", JSON.stringify(valores.lancamento));
      
      
}); 

opts.dados = localStorage.getItem("lanca");
var dados = opts.dados;
opts.dados = JSON.parse(dados);


   }


   divida(e){
    var obj = {};
       var j=0;
       opts.dados = "";
       var axios = require('axios');
       var idCliente = 0;
       var idServidor = 0;
       var str = "28/02/2020";
       var hoy =  new Date();
       var hoyFormatada = hoy.getDate() + '/' + ( hoy.getMonth() + 1 ) + '/' + hoy.getFullYear();
       var dataAtual = new Date(hoyFormatada.split('/').reverse().join('/'));
       var dataDivida = new Date(str.split('/').reverse().join('/'));
   axios.get('https://ifinancia.herokuapp.com/lancamento/despesas/'+localStorage.getItem("idusuario")).then(function(response){
    
      var valores = response.data; 
            localStorage.setItem("divida", JSON.stringify(valores.lancamento));
     
     
      
      
}); 

opts.dados = localStorage.getItem("divida");
var dados = opts.dados;
var dadosS = JSON.parse(dados);

var j = 0;


var date;
var novaData = new Date();
for(var i in dadosS){
    var str = gamb(dadosS[i]['data']);
    date = new Date(str.split('/').join('/'));
    if(date > novaData){
        obj[j] = dadosS[i];
        j++;
    } 
      
}
opts.dados = obj;


   }


    dividaV(e){
        var obj = {};
       var j=0;
       opts.dados = "";
       var axios = require('axios');
   axios.get('https://ifinancia.herokuapp.com/lancamento/despesas/'+localStorage.getItem("idusuario")).then(function(response){
    
      var valores = response.data; 
            localStorage.setItem("divida", JSON.stringify(valores.lancamento));
     
     
      
      
}); 

opts.dados = localStorage.getItem("divida");
var dados = opts.dados;
var dadosS = JSON.parse(dados);

var j = 0;


var date;
var novaData = new Date();
for(var i in dadosS){
    var str = gamb(dadosS[i]['data']);
   
    date = new Date(str.split('/').join('/'));
    
    if(date < novaData){
        obj[j] = dadosS[i];
        j++;
    }
      
}
opts.dados = obj;

   }

tabela(e){

    var jsPDF = require('jspdf');
    require('jspdf-autotable');
    var columns = ["Descrição", "Valor", "Data", "Conta"];
    var data = [];
    for(var i =0;i<=opts.dados.length;i++){
         data.push([opts.dados[i]['description'],opts.dados[i]['valor'],opts.dados[i]['data'],opts.dados[i]['contaCartao']]); 

}
    
        console.log(data);

        var doc = new jsPDF();
        doc.setFontSize(22);
        doc.text("Relatorio", 1, 10);
        doc.autoTable(columns, data);
        doc.save("Relatorios.pdf");




   }

    function gamb(string){
        var s = '';
        var i = 0;
       while(string[i]!='T'){
           if(string[i]=='-'){
               s = s + '/';
           }else{
            s = s+string[i];
           }
          
           i++;
       }
    return s;

    }
    
    deletar(e){
        axios.delete('https://ifinancia.herokuapp.com/lancamento/delete/'+ document.getElementById("formGroupidDelete").value).then(function(response){
    });

     axios.delete('https://ifinancia.herokuapp.com/metas/delete/'+ document.getElementById("formGroupidDelete").value).then(function(response){
    });


    }


</script>
</lancamento>
